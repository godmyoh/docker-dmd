This is fork from https://github.com/lindt/docker-dmd.

See [original Docker Hub page](https://hub.docker.com/r/dlanguage/dmd/).

# Usage

## With Docker

```
$ pwd
/home/your_name/your_project
$ docker run --rm -it -v /home/your_name/your_project:/src registry.gitlab.com/godmyoh/docker-dmd:2.087.1 dub build
```

## With Docker for Windows

Points

- `Shared Drives` settings are required.

```
> cd
C:\Users\your_name\your_project
> docker run --rm -it -v C:\Users\your_name\your_project:/src registry.gitlab.com/godmyoh/docker-dmd:2.087.1 dub build
```

## With Docker for Windows and WSL(Ubuntu)

Points

- `Shared Drives` settings are required.
- If your source files are on Windows, the path on MobyLinuxVM must be passed to `-v` option instead of the path on Ubuntu.
  - Typically, it works well that removing leading `/mnt` from the full path.

```
$ pwd
/mnt/c/Users/your_name/your_project
$ docker run --rm -it -v /c/Users/your_name/your_project:/src registry.gitlab.com/godmyoh/docker-dmd:2.087.1 dub build
```

## With GitLab Runner(GitLab.com)

.gitlab-ci.yml

```
stages:
  - build

build-linux:
  stage: build
  tags:
    - docker
    - shared
  image: registry.gitlab.com/godmyoh/docker-dmd:2.087.1
  script:
    - dub build
```

---

# Original README

The following is the original README.

---

[![Docker pulls](https://img.shields.io/docker/pulls/dlanguage/dmd.svg)](https://hub.docker.com/r/dlanguage/dmd/)
[![Docker Build](https://img.shields.io/docker/automated/dlanguage/dmd.svg)](https://hub.docker.com/r/dlanguage/dmd/)
[![Latest Tag](https://img.shields.io/github/tag/lindt/docker-dmd.svg)](https://hub.docker.com/r/dlanguage/dmd/)

# docker-dmd

Docker Image for DMD the Digital Mars [D](http://dlang.org) Compiler.

## Motivation

Installation of a compiler sometimes is cumbersome. This Docker image should take this pain and allow you to easily switch between Versions of the same compiler and even different compilers.

In case a native installation is required, `curl -fsS https://dlang.org/install.sh | bash -s dmd` could be used.

## Other Compilers

Allows to use all major D Compilers without installation.

| Compiler | Latest Tag |
| -------- | ---------- |
| DMD      | [![Latest Tag](https://img.shields.io/github/tag/lindt/docker-dmd.svg)](https://hub.docker.com/r/dlanguage/dmd/) |
| LDC      | [![Latest Tag](https://img.shields.io/github/tag/lindt/docker-ldc.svg)](https://hub.docker.com/r/dlanguage/ldc/) |
| GDC      | [![Latest Tag](https://img.shields.io/github/tag/lindt/docker-gdc.svg)](https://hub.docker.com/r/dlanguage/gdc/) |

## Usage

Place a `test.d` in your current directory.
```
import std.stdio;

void main() {
    writeln("Hello, World!");
}
```

Then execute
```sh
docker run --rm -ti -v $(pwd):/src dlanguage/dmd dmd -run test.d
```

This should plot:
```
Hello, World!
```

If you are too lazy to create a file, you can compile directly from stdin:

```sh
echo 'void main(){import std.stdio; writeln("Hello World");}' | docker run --rm -i -v $(pwd):/src dlanguage/dmd dmd -run -
```
